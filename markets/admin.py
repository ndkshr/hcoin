from django.contrib import admin

# Register your models here.
from .models import Resource, Angel, Bid, User


admin.site.register(Resource)
admin.site.register(Angel)
admin.site.register(Bid)
admin.site.register(User)
