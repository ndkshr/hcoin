from django.db import models

# Create your models here.
class Resource(models.Model):
    resource_type = models.CharField(max_length=200)
    name = models.CharField(max_length=300)
    amount = models.IntegerField(default=0)
    price = models.IntegerField(default=0)

    def __str__(self):
        return self.name



class Angel(models.Model):
    name = models.CharField(max_length=300)
    hcoin = models.IntegerField(default=0)

class Bid(models.Model):
    bid_id = models.ForeignKey(Resource, on_delete=models.CASCADE)
    angel_id = models.ForeignKey(Angel, on_delete=models.CASCADE)
    bid_amount = models.IntegerField(default=0)

    def __str__(self):
        return self.angel_id + " bids " + self.bid_id

class User(models.Model):
    name = models.CharField(max_length=300)
    angel = models.ForeignKey(Angel, on_delete=models.CASCADE)
    aadhar = models.CharField(max_length=12)
